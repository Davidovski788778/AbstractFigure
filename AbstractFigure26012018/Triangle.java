package AbstractFigure26012018;

public class Triangle extends Figure {

    private double leg;

    public double getLeg() {
        return leg;
    }

    public void setLeg(double leg) {
        this.leg = leg;
    }

    public Triangle(double a, double b) {
        super(a, b);
        leg = Math.min(a, b);
    }

    @Override
    public String getName() {
        return "Triangle";
    }

    @Override
    public double getSquare() {
        return leg * leg / 2;
    }

}
